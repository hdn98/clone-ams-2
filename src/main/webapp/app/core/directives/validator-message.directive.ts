import { Component, Directive, HostBinding, HostListener, Input, OnInit, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'ams-validator-messages',
  styles: [],
  template: ` <pre>{{ messages }}</pre> `,
})
class ValidationMessagesComponent implements OnInit {
  @Input() messages: string = 'messages';
  constructor() {
    // TODO document why this constructor is empty
  }
  ngOnInit() {
    // TODO document why this method 'ngOnInit' is empty
  }
}

@Directive({
  selector: '[jhiValidatorMessage]',
})
export class ValidatorMessageDirective {
  // @HostBinding() color?: string;

  @HostListener('valid')
  getMessage() {
    console.log('valid');
  }

  constructor(private viewContainerRef: ViewContainerRef) {}
}
