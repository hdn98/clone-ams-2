// import { Directive } from '@angular/core';

// @Directive({
//     selector: '[jhiValidatorMessage]',
// })
// export class ToolTipDirective {

// }

import { Directive, Input, TemplateRef, ElementRef, HostListener, ComponentRef } from '@angular/core';
import { ConnectedPosition, Overlay, OverlayPositionBuilder, OverlayRef } from '@angular/cdk/overlay';

import { ComponentPortal } from '@angular/cdk/portal';
import { CustomToolTipComponent } from 'app/shared/tooltip/tooltip.component';

export type TooltipPosition = 'top' | 'left' | 'right' | 'bottom';
export type PositionObject = {
  [key in TooltipPosition]?: ConnectedPosition;
};
@Directive({
  selector: '[customToolTip]',
})
export class ToolTipDirective {
  /**
   * This will be used to show tooltip or not
   * This can be used to show the tooltip conditionally
   */
  @Input() showToolTip: boolean = true;

  positionToolTip: PositionObject = {
    right: {
      originX: 'end',
      originY: 'center',
      overlayX: 'start',
      overlayY: 'center',
    },
    top: {
      originX: 'center',
      originY: 'top',
      overlayX: 'center',
      overlayY: 'bottom',
    },
    left: {
      originX: 'start',
      originY: 'center',
      overlayX: 'end',
      overlayY: 'center',
    },
    bottom: {
      originX: 'center',
      originY: 'bottom',
      overlayX: 'center',
      overlayY: 'top',
    },
  };
  //If this is specified then specified text will be showin in the tooltip
  @Input('customToolTip') text?: string = 'cmmTooltip';

  //If this is specified then specified template will be rendered in the tooltip
  @Input() contentTemplate?: TemplateRef<any>;
  @Input() position?: TooltipPosition = 'top';
  @Input() customClass?: string;
  private _overlayRef?: OverlayRef;

  constructor(private _overlay: Overlay, private _overlayPositionBuilder: OverlayPositionBuilder, private _elementRef: ElementRef) {}

  /**
   * Init life cycle event handler
   */
  ngOnInit() {
    if (!this.showToolTip) {
      return;
    }
    let postitionObject;

    const positionStrategy = this._overlayPositionBuilder
      .flexibleConnectedTo(this._elementRef)
      .withPositions([this.positionToolTip[this.position as TooltipPosition]!]);

    this._overlayRef = this._overlay.create({ positionStrategy });
  }

  /**
   * This method will be called whenever mouse enters in the Host element
   * i.e. where this directive is applied
   * This method will show the tooltip by instantiating the McToolTipComponent and attaching to the overlay
   */
  @HostListener('mouseenter')
  show() {
    //attach the component if it has not already attached to the overlay
    const textSize = (this._elementRef.nativeElement as HTMLDivElement).textContent?.trim()?.split('')?.length! * 8;
    const elSize = this._elementRef.nativeElement.offsetWidth;
    // console.log(elSize, textSize);
    if (textSize > elSize && this._overlayRef && !this._overlayRef.hasAttached()) {
      // console.log(textSize >= elSize);

      const tooltipRef: ComponentRef<CustomToolTipComponent> = this._overlayRef.attach(new ComponentPortal(CustomToolTipComponent));
      tooltipRef.instance.text = this._elementRef.nativeElement.textContent.toString();
      tooltipRef.instance.contentTemplate = this.contentTemplate;
      tooltipRef.instance.position = this.position!;
    }
  }

  /**
   * This method will be called when mouse goes out of the host element
   * i.e. where this directive is applied
   * This method will close the tooltip by detaching the overlay from the view
   */
  @HostListener('mouseleave')
  hide() {
    // console.log('hide tooltip');
    this.closeToolTip();
  }

  /**
   * Destroy lifecycle event handler
   * This method will make sure to close the tooltip
   * It will be needed in case when app is navigating to different page
   * and user is still seeing the tooltip; In that case we do not want to hang around the
   * tooltip after the page [on which tooltip visible] is destroyed
   */
  ngOnDestroy() {
    this.closeToolTip();
  }

  /**
   * This method will close the tooltip by detaching the component from the overlay
   */
  private closeToolTip() {
    setTimeout(() => {
      if (this._overlayRef) {
        this._overlayRef.detach();
      }
    }, 1);
  }
}
