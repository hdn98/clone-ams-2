import { animate, state, style, transition, trigger } from '@angular/animations';
import { DialogRef } from '@angular/cdk/dialog';
import { MediaMatcher } from '@angular/cdk/layout';

import {
  ChangeDetectorRef,
  Component,
  ContentChild,
  EventEmitter,
  HostBinding,
  HostListener,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  inject,
} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { MatSidenav } from '@angular/material/sidenav';
import { Title } from '@angular/platform-browser';
import { ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { AccountService } from 'app/core/auth/account.service';
import { EditDialogComponent } from 'app/entities/agency/info/edit-dialog/edit-dialog.component';
import { LoginService } from 'app/login/login.service';
import { DialogComponent, DialogConfig } from 'app/shared/dialog/dialog.component';

@Component({
  selector: 'ams-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit, OnChanges, OnDestroy {
  isExpandedBoolean = true;
  showSubmenu: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;
  mobileQuery?: MediaQueryList;
  //mock value param for common component
  mockCheck = false;

  private _mobileQueryListener: () => void;
  fb = inject(FormBuilder);
  //default option change
  listOption = [1, 2, 3, 4, 5, 6, 6];
  optionValue: any = 0;
  optionValueInput = '';
  mockFormgroup = this.fb.group({
    mockFormControl: new FormControl('', [Validators.required, Validators.min(2), Validators.max(2)]),
    mockFormControlInput: new FormControl('', [Validators.required, Validators.email]),
  });

  mockTooltipData = 'tooltip';

  constructor(
    private matDialog: MatDialog,
    changeDetectorRef: ChangeDetectorRef,
    private accountService: AccountService,
    private titleService: Title,
    private router: Router,
    media: MediaMatcher,
    private loginService: LoginService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.isExpandedBoolean = this.mobileQuery?.matches!;
  }

  @ViewChild('dialog2Instance', { static: true }) dialog2Instance!: EditDialogComponent;
  @ViewChild('templateDialog', { static: true }) templateDialog!: TemplateRef<any>;

  dialogInstance?: MatDialogRef<DialogComponent, MatDialogConfig<DialogConfig>>;

  ngOnInit(): void {
    // try to log in automatically
    this.accountService.identity().subscribe();

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.updateTitle();
      }
    });
    this.openDialog2();
    setTimeout(() => {
      this.mockTooltipData = 'lorem ipsum dolor sit amet, consectetur';
    }, 100);
  }

  private getPageTitle(routeSnapshot: ActivatedRouteSnapshot): string {
    const title: string = routeSnapshot.data['pageTitle'] ?? '';
    if (routeSnapshot.firstChild) {
      return this.getPageTitle(routeSnapshot.firstChild) || title;
    }
    return title;
  }

  private updateTitle(): void {
    let pageTitle = this.getPageTitle(this.router.routerState.snapshot.root);
    if (!pageTitle) {
      pageTitle = 'Jhipster 3';
    }
    this.titleService.setTitle(pageTitle);
  }

  logOut() {
    this.loginService.logout();
    this.router.navigate(['/login']);
  }
  toggleSideNav(sideNav: MatSidenav) {
    console.log(sideNav);
    console.log(this.mobileQuery?.matches);
  }
  getCurrentSelectValue(fc?: FormControl) {
    console.log('this.optionValue', this.optionValue);
    if (fc) {
      console.log(fc.value);
    }
  }
  getFormGroupInstance() {
    console.log(this.mockFormgroup.valid);
  }
  get2waybindingChanges(modelValue?: string | number) {
    console.log(this.optionValueInput);
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.optionValue);
  }

  //dialog
  openDialog(): void {
    this.dialogInstance = this.matDialog.open(DialogComponent, {
      data: {
        customButtonTemplate: undefined,
        customButtonCancelClick: new EventEmitter<any>(),
        customContentTemplate: this.templateDialog,
        customSubmitButtonClick: null,
      },
    });

    this.dialogInstance.afterClosed().subscribe(result => {});
  }

  openDialog2(): void {
    // setTimeout(() => {
    this.dialog2Instance.openDialog();
    this.dialog2Instance.closeDialog();
    // }, 0);
  }

  closeDialog(): void {}
  ngOnDestroy(): void {
    //do nothing
  }
}
