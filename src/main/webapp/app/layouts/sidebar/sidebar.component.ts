import {
  Component,
  Input,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  AfterContentChecked,
  ChangeDetectorRef,
  ViewEncapsulation,
  OnChanges,
  SimpleChanges,
  HostListener,
} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { of as observableOf } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { SidebarServiceTsService } from './sidebar.service.ts.service';
export interface FileNode {
  name: string;
  type: string;
  children?: FileNode[];
}

/** Flat node with expandable and level information */
export interface TreeNode {
  name: string;
  type: string;
  level: number;
  expandable: boolean;
}

@Component({
  selector: 'ams-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit, AfterContentChecked, OnChanges {
  @ViewChild('sidenav') sidenav!: MatSidenav;
  @Input() isExpanded: boolean = false;
  @Output() isExpandedChange = new EventEmitter<boolean>();

  @Input('isMobileDesign') isMobileDesign?: boolean = false;

  showSubmenu: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;

  /** The TreeControl controls the expand/collapse state of tree nodes.  */
  treeControl!: FlatTreeControl<TreeNode>;

  /** The TreeFlattener is used to generate the flat list of items from hierarchical data. */
  treeFlattener!: MatTreeFlattener<FileNode, TreeNode>;

  /** The MatTreeFlatDataSource connects the control and flattener to provide data. */
  dataSource!: MatTreeFlatDataSource<FileNode, TreeNode>;
  constructor(public sidebarService: SidebarServiceTsService, private cdf: ChangeDetectorRef) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel, this.isExpandable, this.getChildren as any);

    this.treeControl = new FlatTreeControl<TreeNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    this.dataSource.data = this.sidebarService.menus;
  }

  ngOnInit(): void {}

  transformer(node: FileNode, level: number) {
    return {
      name: node.name,
      type: node.type,
      level: level,
      expandable: !!node.children,
    };
  }

  ngAfterContentChecked(): void {
    // this.cdf.markForCheck();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.cdf.detectChanges();
  }

  /** Get the level of the node */
  getLevel(node: TreeNode) {
    return node.level;
  }

  /** Return whether the node is expanded or not. */
  isExpandable(node: TreeNode) {
    return node.expandable;
  }

  /** Get the children for the node. */
  getChildren(node: FileNode) {
    return observableOf(node.children);
  }

  /** Get whether the node has children or not. */
  hasChild(index: number, node: TreeNode) {
    return node.expandable;
  }
  toggleMenu() {
    this.isExpanded = !this.isExpanded;
    this.isExpandedChange.emit(this.isExpanded);
  }
}
