import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SidebarServiceTsService {
  constructor() {}

  menus = [
    {
      name: 'material2',
      type: '',
      children: [
        {
          name: 'src',
          type: '',
          children: [
            {
              name: 'cdk',
              children: [
                { name: 'package.json', type: '' },
                { name: 'BUILD.bazel', type: '' },
              ],
            },
            { name: 'lib', type: '' },
          ],
        },
      ],
    },
    {
      name: 'angular',
      type: '',
      children: [
        {
          name: 'packages',
          type: '',
          children: [
            { name: '.travis.yml', type: '' },
            { name: 'firebase.json', type: '' },
          ],
        },
        { name: 'package.json', type: '' },
      ],
    },
    {
      name: 'angularjs',
      type: '',
      children: [
        { name: 'gulpfile.js', type: '' },
        { name: 'README.md', type: '' },
      ],
    },
  ];
}
