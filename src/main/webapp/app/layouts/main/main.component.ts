import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRouteSnapshot, NavigationEnd, ActivatedRoute } from '@angular/router';

import { AccountService } from 'app/core/auth/account.service';
import { tap } from 'rxjs';

@Component({
  selector: 'ams-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  constructor(private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.checkUser();
  }
  checkUser() {
    this.route.paramMap
      .pipe(
        tap((res: any) => {
          console.log('res_____________', res);
        })
      )
      .subscribe();
  }
}
