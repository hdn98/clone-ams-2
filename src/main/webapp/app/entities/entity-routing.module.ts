import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MainPageComponent } from 'app/layouts/main-page/main-page.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
      {
        path: '',
        component: MainPageComponent,
      },
    ]),
  ],
})
export class EntityRoutingModule {}
