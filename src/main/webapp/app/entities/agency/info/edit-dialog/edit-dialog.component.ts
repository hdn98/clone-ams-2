import { Component, EventEmitter, OnInit, Output, OnChanges, TemplateRef, ViewChild, SimpleChanges, Input } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent, DialogConfig } from 'app/shared/dialog/dialog.component';
import { IAgreement } from '../../agency.model';

@Component({
  selector: 'ams-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss'],
})
export class EditDialogComponent implements OnInit, OnChanges {
  dialogInstance?: MatDialogRef<DialogComponent, MatDialogConfig<DialogConfig>>;
  constructor(private matDialog: MatDialog) {
    //do nothing
  }
  formAgencyEdit: FormGroup = new FormGroup({
    agreementName: new FormControl(null, Validators.required),
    statusDate: new FormControl(null, Validators.required),
    agreementStatus: new FormControl(null, Validators.required),
    company: new FormControl(null, Validators.required),
    department: new FormControl(null, Validators.required),
    accountingDepartment: new FormControl(null, Validators.required),
    contractNumber: new FormControl(null, Validators.required),
    contractSigningDate: new FormControl(null, Validators.required),
    certificateNumber: new FormControl(null, Validators.required),
  });

  @Input() dialogStatus?: boolean = false;
  @Output() dialogStatusChange?: EventEmitter<any> = new EventEmitter();
  @Output() openDialogParam?: EventEmitter<any> = new EventEmitter();
  @Output() closeDialogParam?: EventEmitter<any> = new EventEmitter();

  @ViewChild('templateDialog2', { static: true }) templateDialog2!: TemplateRef<any>;
  @ViewChild('buttonHeaderDialog2', { static: true }) buttonHeaderDialog2!: TemplateRef<any>;

  //danh sách công ty

  //danh sách phòng ban

  //danh sách phòng ban kế toán

  //danh sách cán bộ kế toán

  //danh sách trạng thái thỏa thuận

  ngOnInit(): void {
    //do nothing
  }

  openDialog(): void {
    console.log('click!!!!!!!!!!!!!!!!!!');
    const dialogConfig: DialogConfig = {
      customButtonTemplate: undefined,
      customContentTemplate: this.templateDialog2,
      customHeaderButtonGroup: this.buttonHeaderDialog2,
      customSubmitButtonClick: () => {
        console.log(this.formAgencyEdit.value);
      },
    };
    this.dialogInstance = this.matDialog.open(DialogComponent, {
      data: {
        ...dialogConfig,
      },
    });

    this.dialogInstance.afterClosed().subscribe(result => {
      console.log('dialog 2 has closed successfully');
    });
    // this.openDialogParam?.emit()
  }

  convertAbstractControl(fc: AbstractControl<any, any>): FormControl<any> {
    return fc as FormControl;
  }

  closeDialog(): void {
    this.dialogInstance?.close();
    // this.closeDialogParam?.emit()
  }
  ngOnChanges(changes: SimpleChanges): void {
    //do nothing
  }
}
