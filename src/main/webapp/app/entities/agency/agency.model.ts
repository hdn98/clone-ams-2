export interface IAgreement {
  agreementName: string | undefined;
  statusDate: string | undefined;
  agreementStatus: string | undefined;
  company: string | undefined;
  department: string | undefined;
  accountingDepartment: string | undefined;
  accountant: string | undefined;
  contractNumber: string | undefined;
  contractSigningDate: string | undefined;
  certificateNumber: string | undefined;
}

export class Agreement implements IAgreement {
  constructor(
    public agreementName: string | undefined,
    public statusDate: string | undefined,
    public agreementStatus: string | undefined,
    public company: string | undefined,
    public department: string | undefined,
    public accountingDepartment: string | undefined,
    public accountant: string | undefined,
    public contractNumber: string | undefined,
    public contractSigningDate: string | undefined,
    public certificateNumber: string | undefined
  ) {}

  // You can also add additional methods and functionalities to the class if needed
  // For example:
  public getAgreementInfo(): string {
    return `Agreement Name: ${this.agreementName}, Status Date: ${this.statusDate}, Company: ${this.company}`;
  }
}
