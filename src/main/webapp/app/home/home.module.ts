import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { SidebarComponent } from 'app/layouts/sidebar/sidebar.component';
import { NavbarComponent } from 'app/layouts/navbar/navbar.component';
import { MainPageComponent } from 'app/layouts/main-page/main-page.component';
import { EditDialogComponent } from 'app/entities/agency/info/edit-dialog/edit-dialog.component';

@NgModule({
  imports: [SharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent, SidebarComponent, NavbarComponent, MainPageComponent, EditDialogComponent],
})
export class HomeModule {}
