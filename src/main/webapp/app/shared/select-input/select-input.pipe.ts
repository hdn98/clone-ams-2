import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'selectInput',
})
export class SelectInputPipe implements PipeTransform {
  transform(value: any, propsKey?: string): any {
    if (typeof value === 'object') {
      return propsKey ? value[propsKey] : value;
    }
    return value;
  }
}
