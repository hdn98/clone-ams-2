import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'ams-select-input',
  templateUrl: './select-input.component.html',
  styleUrls: ['./select-input.component.scss'],
})
export class SelectInputComponent implements OnInit {
  constructor() {}

  @Input() label = 'test test ';
  @Input() options: any[] = [];
  @Input() disabled = false;
  @Input() clearOption = false;
  @Input() formControlProps?: FormControl<any>;
  @Input() canbeValidated = false;
  @Input() optionValueObject = '';
  @Input() optionLabelObject = '';
  @Input() errorValidate = '';
  @Input() isMultiple = false;
  @Input() requireValidate = 'cmm';

  @Input() customClass = '';

  @Output() onChange = new EventEmitter();

  @Input() valueProps?: any;
  @Output() valuePropsChange = new EventEmitter();

  //mock data mat-select

  ngOnInit(): void {
    setTimeout(() => {
      for (let i = 0; i < 10; i++) {
        this.options.push({
          value: 'value  ' + i,
          keys: i,
        });
      }
    }, 0);
    this.optionLabelObject = 'value';
    this.optionValueObject = 'keys';
    console.log(this.options);
  }
  checkIsFormControl() {
    return this.formControlProps;
  }
  checkMultiSelectItem(): boolean {
    return true;
  }
  /**
   * clear selected value of select option
   * @param event event default button click
   */
  deleteSelectItem(event: Event) {
    this.valueProps &&= '';
    this.formControlProps?.setValue('');
    event.stopPropagation();
  }
  optionChanged(event?: any, value?: any) {}
}
