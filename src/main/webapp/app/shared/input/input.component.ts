import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';

@Component({
  selector: 'ams-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit {
  listValidator: string[] = [];

  @Input() label = 'test test ';
  @Input() disabled = false;
  @Input() clearOption = false;
  @Input() formControlProps?: FormControl<any>;
  @Input() type: 'text' | 'number' | 'password' | 'email' = 'text';
  @Input() placeholder: string = 'placeholder';

  @Input() canbeValidated = false;
  @Input() errorValidate = '';
  @Input() requireValidate = 'cmm';
  @Input() customClass = '';

  @Output() onChange = new EventEmitter();
  //custom 2 way binding
  @Input() valueProps?: string | number;
  @Output() valuePropsChange = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
    //get list validator error
  }
  getErrorMessage() {
    return this.requireValidate;
  }
}
