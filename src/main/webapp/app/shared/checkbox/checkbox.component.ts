import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ams-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
})
export class CheckboxComponent implements OnInit {
  constructor() {}
  @Input() disabled = false;
  @Input() label = '';
  @Input() labelPosition: 'after' | 'before' = 'before';
  @Input() inlineClass = '';

  @Input() checkedProps = false; // custom 2 way binding
  @Output() checkedPropsChange = new EventEmitter<boolean>();
  //custom 2 way binding : phai cung ten input va output change

  @Output() onClick = new EventEmitter();

  ngOnInit(): void {}
  toggleChange() {
    this.checkedPropsChange.emit(this.checkedProps);
  }
}
