import { AfterContentChecked, Component, EventEmitter, Input, OnInit, Output, ViewChild, TemplateRef } from '@angular/core';

export type ButtonType = 'basic' | 'primary' | 'accent' | 'warn' | 'disabed' | 'link';

@Component({
  selector: 'ams-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  constructor() {}

  @Input() customClass = 'default-button';
  @Input() buttonId = '';
  @Input() buttonType: ButtonType = 'primary';
  @Input() icon = '';
  @Input() buttonTitle = '';
  @Input() disabled = false;
  @Input() customTemplate?: TemplateRef<any>;

  @Output() clickEvent: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {}
}
