import { Component, EventEmitter, Inject, OnInit, Optional, TemplateRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

export interface DialogConfig {
  customButtonTemplate?: TemplateRef<any>;
  customButtonCancelClick?: () => void | Promise<any>;
  customContentTemplate?: TemplateRef<any>;
  customSubmitButtonClick?: () => void | Promise<any>;
  dialogTitle?: string;
  positionFooterBtn?: 'start' | 'center' | 'end';
  customHeaderButtonGroup: TemplateRef<any>;
}

@Component({
  selector: 'ams-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent implements OnInit {
  constructor(
    @Optional()
    public dialogRef: MatDialogRef<DialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: DialogConfig
  ) {}

  ngOnInit(): void {
    console.log('data!', this.data);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  submitClick() {
    this.data.customSubmitButtonClick?.();
  }
}
