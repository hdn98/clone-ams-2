import { NgModule } from '@angular/core';

import { SharedLibsModule } from './shared-libs.module';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { DurationPipe } from './date/duration.pipe';
import { FormatMediumDatetimePipe } from './date/format-medium-datetime.pipe';
import { FormatMediumDatePipe } from './date/format-medium-date.pipe';
import { SortByDirective } from './sort/sort-by.directive';
import { SortDirective } from './sort/sort.directive';
import { ItemCountComponent } from './pagination/item-count.component';
import { FilterComponent } from './filter/filter.component';
import { ButtonComponent } from './button/button.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { DialogComponent } from './dialog/dialog.component';
import { SelectInputComponent } from './select-input/select-input.component';
import { TableComponent } from './table/table.component';
import { StepperComponent } from './stepper/stepper.component';
import { ToastComponent } from './toast/toast.component';
import { ToastMessageComponent } from './toast-message/toast-message.component';
import { InputSearchComponent } from './input-search/input-search.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

import { CalendarComponent } from './calendar/calendar.component';
import { ValidatorMessageDirective } from 'app/core/directives/validator-message.directive';
import { SelectInputPipe } from './select-input/select-input.pipe';
import { InputComponent } from './input/input.component';
import { ToolTipDirective } from 'app/core/directives/tooltip.directive';
import { CustomToolTipComponent } from './tooltip/tooltip.component';

const shareNotImport = [
  HasAnyAuthorityDirective,
  SortByDirective,
  SortDirective,
  ValidatorMessageDirective,
  DurationPipe,
  FormatMediumDatetimePipe,
  FormatMediumDatePipe,
  SelectInputPipe,
  ToolTipDirective,
  CustomToolTipComponent,

  CalendarComponent,
  ItemCountComponent,
  AlertComponent,
  AlertErrorComponent,
  ButtonComponent,
  FilterComponent,
  CheckboxComponent,
  DialogComponent,
  SelectInputComponent,
  TableComponent,
  StepperComponent,
  ToastComponent,
  ToastMessageComponent,
  InputComponent,
  InputSearchComponent,
  BreadcrumbComponent,
];
@NgModule({
  imports: [SharedLibsModule],
  declarations: [...shareNotImport],
  exports: [...shareNotImport, SharedLibsModule],
})
export class SharedModule {}
